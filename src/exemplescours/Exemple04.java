package exemplescours;

import formesgeo.Boite;
import formesgeo.Piece;

public class Exemple04 {
    
    public static void main(String[] args) {
           
        Boite uneBoite = new Boite();
        
         for (Piece p : uneBoite.getLesPieces()){
        
             p.afficher();
        }
        
        System.out.printf("\nPoids total des pièces: %5.2f g\n\n",
                uneBoite.poidsTotal());   
    }

}



