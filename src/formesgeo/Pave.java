package formesgeo;

public class Pave extends Piece {

    private double largeur,hauteur, profondeur;
	
    public Pave( String repere,  String couleur, double densite,
                 double largeur, double hauteur, double profondeur){
           
      super(repere,couleur, densite);
	   
      this.largeur    = largeur;
      this.hauteur    = hauteur;
      this.profondeur = profondeur;
    }

    @Override
    public void afficher(){
       
      System.out.print("\nPAVE ");
     
      super.afficher();
       
      System.out.printf(
                        "Largeur:%6.2f cm Hauteur: %6.2f cm Profondeur: %6.2f cm\n",
                        largeur, hauteur, profondeur);
     
      System.out.printf("Volume: %6.2f cm3 Poids:  %6.2f g\n" , volume(), poids());
    }
       
    @Override
    public double volume(){ 
    
        return largeur*hauteur*profondeur;
    }

    //<editor-fold defaultstate="collapsed" desc="Getters et Setters">
    
    public double getLargeur() {
        return largeur;
    }
    
    public void setLargeur(double largeur) {
        this.largeur = largeur;
    }
    
    public double getHauteur() {
        return hauteur;
    }
    
    public void setHauteur(double hauteur) {
        this.hauteur = hauteur;
    }
    
    public double getProfondeur() {
        return profondeur;
    }
    
    public void setProfondeur(double profondeur) {
        this.profondeur = profondeur;
    }
    //</editor-fold>  
   }



