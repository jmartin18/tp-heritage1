package formesgeo;
import java.util.LinkedList;
import java.util.List;

public class Boite {

    private List<Piece> lesPieces= new LinkedList();

    public Boite(){

      Pave pav1,pav2;
      Cylindre cyl1;
      Sphere sph1, sph2;
      Cone con1;
      
      pav1  = new Pave ("A","vert", 7.87 , 1  , 3  , 1);
      pav2  = new Pave ("B","rouge", 4.43 , 1.5, 2.5, 1.50);
      cyl1  = new Cylindre("C", "bleu", 2.70 , 2, 3);  
      sph1 = new Sphere("D", "rouge", 2.70, 3.50);
      sph2 = new Sphere("E", "bleu", 4.43, 2.00);
      con1 = new Cone("G", "vert", 4.43, 2.00, 3.50);
      
        
      lesPieces.add(pav1);
      lesPieces.add(pav2);
      lesPieces.add(cyl1);
      lesPieces.add(sph1);
      lesPieces.add(sph2);
      lesPieces.add(con1);
    }

    public double poidsTotal(){
     
     double valRet=0.0;
     
     for(Piece p : this.lesPieces) {
     
         valRet += p.poids();
     }
     
     return valRet;
    }

    public List<Piece> getLesPieces() {
        
        return lesPieces;
    }
}



