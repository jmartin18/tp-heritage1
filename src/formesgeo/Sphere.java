 package formesgeo;
 import static java.lang.Math.*;
 
 public class Sphere extends Piece {

   private double rayon;
  
   public Sphere( String repere, String couleur, double densite, double rayon ) {
  
       super(repere,couleur, densite);
       this.rayon=rayon;
  }

   @Override
   public double volume() { return 3*(PI * pow(rayon,3))/4;}

    @Override
    public void afficher() {
        super.afficher(); 
        System.out.printf("Rayon : %6.2f \n", rayon);
        System.out.printf("Volume : %6.2f cm3\n Poids : %6.2f g", volume(), poids());
    }
 }