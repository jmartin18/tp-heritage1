
package exercicestp;

import formesgeo.Boite;
import formesgeo.Piece;

public class Exo04 {

    public static void main(String[] args) {
        Boite b = new Boite();
        Double poidsTot = 0d;
        for(Piece p : b.getLesPieces()){
            if(p.getCouleur() == "rouge"){
                p.afficher();
                System.out.println("");
                poidsTot += p.poids();
            }
        }
        System.out.printf(" \nL'ensemble des pièces rouge pèse  : %5.2f g\n",poidsTot);
        
    }
}
