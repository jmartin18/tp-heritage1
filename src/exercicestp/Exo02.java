package exercicestp;

import formesgeo.Boite;
import formesgeo.Piece;

public class Exo02 {
  
    // ATTENTION !
    // VOUS NE DEVEZ PAS MODIFIER LE CODE DE CETTE CLASSE
    // LE TRAVAIL A FAIRE DOIT-ÊTRE FAIT DANS LA CLASSE BOITE
    
    public static void main(String[] args) {

        Boite uneBoite= new Boite();
        
        for (Piece p : uneBoite.getLesPieces()){
        
             p.afficher();
             System.out.println("\n");
        } 
       
        System.out.printf("\nPoids total des pièces: %5.2f g\n\n",
                           uneBoite.poidsTotal());    
        System.out.println();
        
    }
}

