
package exercicestp;

import formesgeo.Boite;
import formesgeo.Piece;

public class Exo03 {

    public static void main(String[] args) {
        Boite b = new Boite();
        for(Piece p : b.getLesPieces()){
            if(p.poids() > 100){
                p.afficher();
                System.out.println("");
            }
            
        }
    }
}
