
package exercicestp;

import formesgeo.Boite;
import formesgeo.Piece;
import formesgeo.Sphere;

public class Exo05 {

    public static void main(String[] args) {
        Boite b = new Boite();
        for(Piece p : b.getLesPieces()){
            if(p instanceof Sphere && p.getCouleur()=="rouge"){
                p.afficher();
                System.out.println("");
            }
        }
    }
}
