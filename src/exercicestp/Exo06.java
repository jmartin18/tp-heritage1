
package exercicestp;

import formesgeo.Boite;
import formesgeo.Piece;

public class Exo06 {

    public static void main(String[] args) {
        Boite b = new Boite();
        Piece pleger = null;
        Double pmin = Double.MAX_VALUE;
        for(Piece p : b.getLesPieces()){
            if(p.poids() < pmin){
                pmin = p.poids();
                pleger = p;
            }
        }
        pleger.afficher();
    }
}
