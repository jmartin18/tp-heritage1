
package exercicestp;

import formesgeo.*;

public class Exo01 {

    public static void main(String[] args) {
        Cone cone1;
        Sphere sphere1, sphere2;
         
        cone1 = new Cone("F", "vert", 4.43, 2, 3.50);
        sphere1 = new Sphere("D", "rouge", 2.70, 3.50);
        sphere2 = new Sphere("E", "rouge", 4.43, 2);
        
        sphere1.afficher();
        sphere2.afficher();
        cone1.afficher();
    }
}
